<?php session_start(); ?>
<?php require_once './includes/functions.php'; ?>

<?php display_html_header($args = array("title" => "Početna stranica")); ?>

    <?php if (isset($_GET["logout"])) {
        session_destroy();
        header("Location: index.php");
    } ?>
    <body>
        <div id="container">
        <header>
            <?php require_once './includes/menu.php'; ?>
        </header>
        
        <main>
            <?php
                $link = connect_to_db();
                $sql = "SELECT * FROM novosti";
                $result = mysqli_query($link, $sql);
                $result_list = array();
                while ($row = mysqli_fetch_array($result)) {
                    $result_list[] = $row;
                }
                foreach ($result_list as $row) {
                    $naslov = stripslashes($row["naslov"]);
                    $sadrzaj = stripslashes($row["sadrzaj"]);
                    echo "<article>\n";
                    echo "<h3>$naslov</h3>\n";
                    echo "<p>$sadrzaj</p>\n";
                    echo "</article>\n";
                }
            ?>
        </main>
        
<?php display_html_footer($args = array()) ?>