<?php session_start();?>
<?php require_once './includes/functions.php'; ?>

<?php display_html_header($args = array("title" => "Login")); ?>
        <?php
        
        // Testiramo da li je forma poslana, dovoljno je samo jedno polje za test.
        if (isset($_POST["username"])){
            
            // Polja username i password moraju biti popunjena da bi 
            // krenuli u postupak autentifikacije podataka
            if(empty($_POST["username"]) OR empty($_POST["password"])){
                    login_error("Username ili password nije unesen.");
            } else {

                // connecting to database
                $db_connection = connect_to_db();

                // uzimamo podatke o korisniku na temelju username-a
                $sql = "SELECT * FROM users WHERE username =  '" . $_POST["username"] . "'";
                $result = mysqli_query($db_connection, $sql);
                $row_num = mysqli_num_rows($result);
                
                // ako je query vratio redak, tj. username postoji
                if($row_num){
                    $row = mysqli_fetch_assoc($result);

                    if ($row["password"] == $_POST["password"]){
                        // password je tocan, spremamo podatke u $_session
                        $_SESSION["user_ID"] = $row["user_ID"];
                        $_SESSION["username"] = $row["username"];
                        header("Location: index.php");
                    } else {
                        login_error("Krivi password.");
                    }
                    
                } else {
                    login_error("Krivi username.");
                }
                
            }
             
        } else { ?>
        <body>
            <div id="container">
            <header>
                <?php require_once './includes/menu.php'; ?>
            </header>
            
            <main>
                <form method="POST" action="login.php" name="loginForm" id="loginForm">
                    <table>
                        <tr>
                            <td>username:</td>
                            <td><input type="text" name="username"></td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td><input type="password" name="password"></td>                                       
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="LOGIN">
                            </td>
                        </tr>

                    </table>
                </form>
            </main>
        <?php } ?>
            
<?php display_html_footer($args = array()) ?>