<?php session_start();?>
<?php require_once './includes/functions.php'; ?>

<?php display_html_header($args = array("title" => "Registriraj se!", "js_file" => "register.js")); ?>    
        <?php
            $db_connection = connect_to_db();
            
            if (isset($_POST["tryRegister"])){
                if (!(!empty($_POST["username"]) && !empty($_POST["password1"]) && !empty($_POST["password2"]) && !empty($_POST["ime"]) && !empty($_POST["prezime"]) && !empty($_POST["e-mail"]) && !empty($_POST["post_broj"]))) {
                    printf("upisi obavezne podatke<br> ");
                }  else                  
                    if ($_POST["password1"] != $_POST["password2"]) 
                        printf("passwordi su razliciti!<br> ");
                    else{
                        $sql = sprintf("SELECT COUNT(username) AS broj FROM users WHERE username LIKE '%s';", $_POST["username"]);
                        $result = mysqli_query($db_connection, $sql);
                        if (mysqli_fetch_array($result)["broj"] != 0)
                            printf("postoji taj username<br>");
                        else {
                            $sql1 = "SELECT MAX(user_ID) AS najveci FROM users;";
                            $sql2 = "SELECT MAX(user_ID) AS najveci FROM user_details;";
                            $najveci = max( mysqli_fetch_assoc(mysqli_query($db_connection, $sql1))["najveci"], 
                                            mysqli_fetch_assoc(mysqli_query($db_connection, $sql2))["najveci"]);
                    
                            $sql1 = sprintf("INSERT INTO users (user_ID, username, password) VALUES ( '%d', '%s', '%s');", 
                                            $najveci + 1, $_POST["username"], $_POST["password1"] );
                            $sql2 = sprintf("INSERT INTO user_details (`user_ID`, `ime`, `prezime`, `telefon`, `e-mail`, `pr_ID`, `post_broj`, `ulica`, `kucni_broj`) VALUES (%d, '%s', '%s', '%s', '%s', %d, %d, '%s', %d);", $najveci + 1, $_POST["ime"], $_POST["prezime"], $_POST["telefon"], $_POST["e-mail"], (int)$_POST["prometalo"], $_POST["post_broj"], $_POST["ulica"], $_POST["kucni_broj"]);
                                    
                                    
                            //printf("%s", $sql2);
                            echo sprintf("%d" , mysqli_query($db_connection, $sql1));
                            echo sprintf("%d" , mysqli_query($db_connection, $sql2));
                              echo mysqli_error($db_connection);  
                            
                        }
                
                    }
            }
            
            
        ?>
        <div id="container">
            <header>
                <?php require_once './includes/menu.php'; ?>
            </header>
            <main>
                <form action="register.php" method="post">
                <table>
                    <tr>
                        <td>*Username:</td>
                        <td><input type="text" name="username" <?php if (isset($_POST["username"])) printf("value=%s", $_POST["username"]);?> ></td>
                    </tr>
                    <tr>
                        <td>*Password:</td>
                        <td><input type="text" name="password1"></td>
                    </tr>
                    <tr>
                        <td>*Repeat password:</td>
                        <td><input type="text" name="password2"></td>
                    </tr>
                    <tr>
                        <td>*Ime:</td>
                        <td><input type="text" name="ime"  <?php if (isset($_POST["ime"])) printf("value=%s", $_POST["ime"]);?>></td>
                    </tr>
                    <tr>
                        <td>*Prezime:</td>
                        <td><input type="text" name="prezime" <?php if (isset($_POST["prezime"])) printf("value=%s", $_POST["prezime"]);?>></td>
                    </tr>
                    <tr>
                        <td>*e-mail:</td>
                        <td><input type="text" name="e-mail" <?php if (isset($_POST["e-mail"])) printf("value=%s", $_POST["e-mail"]);?>></td>
                    </tr>
                    <tr>
                        <td>Telefon:</td>
                        <td><input type="text" name="telefon" <?php if (isset($_POST["telefon"])) printf("value=%s", $_POST["telefon"]);?>></td>
                    </tr>
                    <tr>
                        <td>Prometalo:</td>
                        <td>
                            <select name="prometalo">
                                <?php

                                    $sql = "SELECT * FROM prometalo";
                                    $result = mysqli_query($db_connection, $sql);
                                    while ( $row = mysqli_fetch_assoc($result) ){
                                        printf ("<option value=\"%d\">%s</option>", $row["pr_ID"], $row["ime_prometala"]);
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>*Postanski broj:</td>
                        <td><input type="number" name="post_broj" <?php if (isset($_POST["post_broj"])) printf("value=%s", $_POST["post_broj"]);?>></td>
                    </tr>
                    <tr>
                        <td>Ulica:</td>
                        <td><input type="text" name="ulica" <?php if (isset($_POST["ulica"])) printf("value=%s", $_POST["ulica"]);?>></td>
                    </tr>
                    <tr>
                        <td>Kucni broj:</td>
                        <td><input type="number" name="kucni_broj" <?php if (isset($_POST["kucni_broj"])) printf("value=%s", $_POST["kucni_broj"]);?>></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Šalji!"></td>

                    </tr>
                </table>
                    <input type="hidden" name="tryRegister">
                </form>
            </main>
<?php display_html_footer($args = array()) ?>
