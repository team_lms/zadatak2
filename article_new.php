<?php session_start();?>
<?php require_once './includes/functions.php'; ?>

<?php display_html_header($args = array("title" => "Registriraj se!", "js_file" => "register.js")); ?>    


<div id="container">
    <header>
        <?php require_once './includes/menu.php'; ?>
    </header>
    <main>
        <?php
            //da li je korisnik probao submitati formu?
            if (isset($_POST["trySubmit"])){
                //echo "primio submit";
                $dobarClanak = TRUE;
                if (empty($_POST["naslov"])){
                    printf("clanak mora imati naslov<br>");
                    $dobarClanak = FALSE;
                }
                if (empty($_POST["sadrzaj"])){
                    printf("clanak mora imati sadrzaj<br>");
                    $dobarClanak = FALSE;
                }
                //posalji upit u bazu ako je sve uredu
                if ($dobarClanak){
                    $db_connection = connect_to_db();
                    $sql = sprintf("INSERT INTO novosti (user_ID, naslov, sadrzaj) VALUES (%d, '%s', '%s')", $_SESSION["user_ID"], $_POST["naslov"], $_POST["sadrzaj"]);
                    if (mysqli_query($db_connection, $sql) === TRUE)
                        printf("Clanak uspjesno uploadan!");

                }
            }
            //else{echo "nisam primio";}

        ?>
        
        <?php 
            //provjeri da li korisnik ima pristup izradi novih clanaka
            if (isset($_SESSION["user_ID"])){
        ?>
        <!-- fomrma za upis novog clanka -->
        <form action="article_new.php" method="POST">
            <table>
                <tr><td>Naslov:</td>
                <tr><td><input type="text" name="naslov" <?php if (isset($_POST["trySubmit"])) printf("value=\"%s\"", $_POST["naslov"]);?>></td></tr>
                <tr><td>Sadrzaj:</td>
                <tr>
                    <td>
                        <textarea name="sadrzaj" rows="10" cols="100%" "><?php if (isset($_POST["trySubmit"])) printf("%s", $_POST["sadrzaj"]); ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="trySubmit">
                        <input type="submit" value="SUBMIT!">
                    </td>
                </tr>
            </table>
        </form>
        <?php }else echo "nemas pristup ovoj stranici";?>
    </main>    
<?php display_html_footer($args = array()) ?>
