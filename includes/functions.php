<?php 

function connect_to_db() {
    require_once(dirname(__FILE__) . "/" . "../config/db_config.php");

    $conn = mysqli_connect($db['servername'], $db['username'], $db['password'], $db['db_name']);
    if (!$conn){
        die("Connection failed: " . mysqli_connect_error());
    }
    return $conn;
}

function display_html_header($args = array()) {
    if(isset($args["js_file"])) { $js_file_link = "<script src=\"{$args["js_file"]}\" type=\"text/javascript\"></script>"; }
    else { $js_file_link = NULL; }
    
    echo "
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset=\"UTF-8\">
            <title>{$args["title"]}</title>
                <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />
                <!-- <script src=\"script.js\" type=\"text/javascript\"></script> -->
                $js_file_link
        </head>
    ";
}

function display_html_footer($args = array()) {
    echo "
            <footer>
                <menu id=\"navigation\">
                    <ul>
                        <li><a href=\"index.php\">Homepage</a></li>
                        <li><a href=\"login.php\">Login</a></li>
                        <li><a href=\"register.php\">Registration</a></li>
                    </ul>
                </menu>
            </footer>
            </div>
        </body>
    </html>
    ";
}

function login_error($err_msg) {
    echo $err_msg . "<br />";
    echo '<a href = "login.php">Natrag na login.</a>';
}

?>